package org.matrix

import org.junit.jupiter.api.Test
import org.mockito.Mockito
import org.mockito.Mockito.mock

class MatrixTests {
    @Test
    fun `Mockito for Exception`() {
        val m = mock(Matrix::class.java)

        try {
            fillMatrix(0, m)
        } catch (ex: IllegalArgumentException) {
            ex.message
        }

        try {
            fillMatrix(-1, m)
        } catch (ex: IllegalArgumentException) {
            ex.message
        }
    }

    @Test
    fun `Mockito for 1x1`() {
        val m = mock(Matrix::class.java)

        fillMatrix(1, m)

        Mockito.verify(m).set(1, 1, 1)
    }

    @Test
    fun `Mockito for 4x4`() {
        val m = mock(Matrix::class.java)

        fillMatrix(4, m)

        Mockito.verify(m).set(4, 1, 1)
        Mockito.verify(m).set(4, 2, 2)
        Mockito.verify(m).set(4, 3, 3)
        Mockito.verify(m).set(4, 4, 4)
        Mockito.verify(m).set(3, 4, 5)
        Mockito.verify(m).set(2, 4, 6)
        Mockito.verify(m).set(1, 4, 7)
        Mockito.verify(m).set(1, 3, 8)
        Mockito.verify(m).set(1, 2, 9)
        Mockito.verify(m).set(1, 1, 10)
        Mockito.verify(m).set(2, 1, 11)
        Mockito.verify(m).set(2, 2, 12)
        Mockito.verify(m).set(2, 3, 13)
        Mockito.verify(m).set(3, 3, 14)
        Mockito.verify(m).set(3, 2, 15)
        Mockito.verify(m).set(3, 1, 16)
    }

    @Test
    fun `Mockito for 5x5`() {
        val m = mock(Matrix::class.java)

        fillMatrix(5, m)

        Mockito.verify(m).set(5, 1, 1)
        Mockito.verify(m).set(5, 2, 2)
        Mockito.verify(m).set(5, 3, 3)
        Mockito.verify(m).set(5, 4, 4)
        Mockito.verify(m).set(5, 5, 5)
        Mockito.verify(m).set(4, 5, 6)
        Mockito.verify(m).set(3, 5, 7)
        Mockito.verify(m).set(2, 5, 8)
        Mockito.verify(m).set(1, 5, 9)
        Mockito.verify(m).set(1, 4, 10)
        Mockito.verify(m).set(1, 3, 11)
        Mockito.verify(m).set(1, 2, 12)
        Mockito.verify(m).set(1, 1, 13)
        Mockito.verify(m).set(2, 1, 14)
        Mockito.verify(m).set(2, 2, 15)
        Mockito.verify(m).set(2, 3, 16)
        Mockito.verify(m).set(2, 4, 17)
        Mockito.verify(m).set(3, 4, 18)
        Mockito.verify(m).set(4, 4, 19)
        Mockito.verify(m).set(4, 3, 20)
        Mockito.verify(m).set(4, 2, 21)
        Mockito.verify(m).set(4, 1, 22)
        Mockito.verify(m).set(3, 1, 23)
        Mockito.verify(m).set(3, 2, 24)
        Mockito.verify(m).set(3, 3, 25)
    }
}
